﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrlManufacturingWebSite.Entities;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using DRL.Utilities.ValidationUtilities;

namespace DrlManufacturingWebSite.Tests.Models
{
    [TestClass]
    public class Validations
    {
        [TestMethod]
        public void IsValidUserEmail()
        {
            string email = "jose_mgomez@hotmail.com";
            Assert.IsTrue(RegexValidations.IsEmailAddressValid(email));
        }

        [TestMethod]
        public void IsPasswordHashSameAsUserHashedPassword()
        {
            string inputString = "josemigp22jj33";
            byte[] data = System.Text.Encoding.ASCII.GetBytes(inputString);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            String hash = System.Text.Encoding.ASCII.GetString(data);
            Assert.AreEqual("??1???6,<0???N[M{Pz???=Cs?n7E??", hash);
        }

        [TestMethod]
        public void IsTextFormattedAsCedula()
        {
            string text = "023-0075764-4";
            string rex = @"^[0-9]{3}-?[0-9]{7}-?[0-9]{1}$";
            Assert.IsTrue(Regex.IsMatch(text, rex));
        }

    }
}
