﻿using DrlManufacturingWebSite.DataLayer;
using DrlManufacturingWebSite.Models;
using DrlManufacturingWebSite.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DrlManufacturingWebSite.Controllers
{
    public class RecursosHumanosController : Controller
    {
        //
        // GET: /RecursosHumanos/

        public ActionResult Index()
        {
            return View();
        }



        [HttpPost]
        [Authorize]
        public ActionResult EnviarPerfil(DatosPersonalesModel model, HttpPostedFileWrapper FotoString)
        {
            
            if (ModelState.IsValid )
            {
                byte[] fileData = null;
                //model.Foto = Convert.FromBase64String(model.FotoString);
                if (FotoString != null)
                {
                    
                    using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
                    {
                        fileData = binaryReader.ReadBytes(FotoString.ContentLength);
                        model.Foto = fileData;
                    }
                    
                }
                
                EFDataLayer dataLayer = new EFDataLayer();
                model.UserID = dataLayer.GetUserByUsername(User.Identity.Name).UserId;
                if(FotoString == null)
                {
                    var previosPic = dataLayer.GetDatosPersonales(model.UserID);
                    if (previosPic == null || previosPic.Foto == null)
                    {
                        ModelState.AddModelError("FotoString", "Por favor seleccione una foto");
                        return View();
                    }
                }
                dataLayer.SaveDatosPersonales(model);

               return RedirectToAction("DeclaracionSalud");
            }
            else
            {
                return View();
            }
            
        }
        
       
        [HttpGet]
        [Authorize]
        public ActionResult EnviarPerfil()
        {
            DatosPersonalesModel model = null;
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                EFDataLayer dataLayer = new EFDataLayer();
                model =  dataLayer.GetDatosPersonales(dataLayer.GetUserByUsername(User.Identity.Name).UserId);
            }

            return View(model);
        }



        [HttpPost]
        [Authorize]
        public ActionResult DeclaracionSalud(DeclaracionSaludModel model, string submit)
        {
            if(ModelState.IsValid)
            {
                EFDataLayer datalayer = new EFDataLayer();
                model.UserId = datalayer.GetUserByUsername(User.Identity.Name).UserId;
                datalayer.SaveDeclaracionSalud(model);
                
                string action = "";
                if (model.Validate())
                {
                    switch (submit)
                    {
                        case "Previo":
                            {
                                action = "EnviarPerfil";
                            }break;
                        case "Proximo":
                            {
                                action = "GrupoFamiliar";
                            }break;
                        default:
                            {
                                return View();
                            }
                    }
                }
                else
                {
                    ModelState.AddModelError("Padecimiento", "Si ha seleccionado que padece de alguna enfermedad o alergía, por favor especifícar");
                    return View();
                }

                return RedirectToAction(action);
            }
            else
            {
                return View();
            }
            
        }

        [HttpGet]
        [Authorize]
        public ActionResult GrupoFamiliar()
        {
            GrupoFamiliarModel model = null;
            EFDataLayer datalayer = new EFDataLayer();
            model = datalayer.GetGrupoFamiliar(datalayer.GetUserByUsername(User.Identity.Name).UserId);
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult GrupoFamiliar(GrupoFamiliarModel model, string submit)
        {
            if (ModelState.IsValid)
            {
                var emptyList = model.Hermanos.Where(h => string.IsNullOrEmpty(h)).ToList();
                foreach (string s in emptyList)
                {
                    model.Hermanos.Remove(s);
                }
                if (submit.ToLower() == "agregar")
                {
                 
                    var her = model.Hermanos.GroupBy(x => x)
                        .Where(group => group.Count() > 1)
                        .Select(group => group.Key);
                    if (her != null)
                    {
                        foreach(string s in her)
                        {
                            model.Hermanos.Remove(model.Hermanos.Where(c => c == s).First());
                        }
                        
                    }
                    if(model.Hermanos.Count > 6)
                    {
                     
                        model.Hermanos.RemoveAt(model.Hermanos.Count - 1);
                        ModelState.AddModelError("Hermanos", "Puede ingresar un máximo de 6 hermanos");
                    }
                    return View(model);
                }
                bool isEdadesZero = false;
                if(model.EdadesHijos != null)
                {
                    isEdadesZero = model.EdadesHijos.Where(e => e == 0).Any();
                }
              
                if ((model.EdadesHijos!= null && model.EdadesHijos.Count < model.HijosACargo) || isEdadesZero)
                {
                    ModelState.AddModelError("EdadesHijos", "Debe de digitar las edades de todos los hijos a cargo");
                    return View(model);
                }

                EFDataLayer datalayer = new EFDataLayer();
                model.UserID = datalayer.GetUserByUsername(User.Identity.Name).UserId;
                datalayer.SaveGrupoFamiliar(model);
                string action = "";
                switch (submit)
                {
                    case "Previo":
                        {
                            action ="DeclaracionSalud";
                        }break;
                    case "Proximo":
                        {
                            action = "EstudiosRealizados";
                        }break;
                }
                return RedirectToAction(action);
            }
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult PersonalIndustrial()
        {
            return View();
        }


        [HttpPost]
        [Authorize]
        public ActionResult PersonalIndustrial(PersonalIndustrial model, string submit)
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult ExperienciaLaboral()
        {
            List<ExperienciaLaboralModel> experienciasLaborales = new List<ExperienciaLaboralModel>();
            ViewBag.Experiencias = experienciasLaborales;
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult ExperienciaLaboral(ExperienciaLaboralModel model, string submit)
        {
            List<ExperienciaLaboralModel> experienciasLaborales = new List<ExperienciaLaboralModel>();
            experienciasLaborales.Add(model);
            ViewBag.Experiencias = experienciasLaborales;
            return View();
        }

        public ActionResult _ExperienciasLaborales(List<ExperienciaLaboralModel> model)
        {
            return View(model);
        }



        [HttpPost]
        [Authorize]
        public ActionResult EstudiosRealizados(EstudiosRealizadosModel model, string submit, Idiomas idiomas)
        {
            Session["EstudiosRealizados"] = model;
            var emptyTitles = model.TitulosObtenidos.Where(m => string.IsNullOrEmpty(m)).ToList();
            var emptyProgramas = model.Programas.Where(m => string.IsNullOrEmpty(m)).ToList();
            foreach (string s in emptyTitles)
            {
                model.TitulosObtenidos.Remove(s);
            }
            foreach (string s in emptyProgramas)
            {
                model.Programas.Remove(s);
            }
            model.Idiomas.Add(idiomas);
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult EstudiosRealizados()
        {
            EstudiosRealizadosModel model = null;
            if (Session["EstudiosRealizados"] != null)
            {
                model = (EstudiosRealizadosModel)Session["EstudiosRealizados"];
            }
            return View(model);
        }

        [Authorize]
        public ActionResult _idiomas()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult DeclaracionSalud()
        {
            DeclaracionSaludModel model = null;
            EFDataLayer datalayer = new EFDataLayer();
            model = datalayer.GetDeclaracionSalud(datalayer.GetUserByUsername(User.Identity.Name).UserId);
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SometerPerfil(DatosPersonalesModel model)
        {
            if (ModelState.IsValid)
            {

            }
            return View();
        }

    }
}
