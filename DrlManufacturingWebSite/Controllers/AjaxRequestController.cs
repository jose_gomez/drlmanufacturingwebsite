﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DrlManufacturingWebSite.IOC;
using DrlManufacturingWebSite.Contracts;

namespace DrlManufacturingWebSite.Controllers
{
    public class AjaxRequestController : Controller
    {
        //
        // GET: /AjaxRequest/

        public JsonResult VerifyUserName(string userName)
        {
            IUsersRepository usersRepo  = TempContainer.GetDefaultRepository();
            bool exists = false;
            exists = (usersRepo.GetUserByUsername(userName) != null);
            return Json(new { Exists = exists }, JsonRequestBehavior.AllowGet);
        }

    }
}
