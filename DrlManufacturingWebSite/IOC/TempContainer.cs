﻿using DrlManufacturingWebSite.Contracts;
using DrlManufacturingWebSite.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DrlManufacturingWebSite.IOC
{
    public class TempContainer
    {
        public static IUsersRepository GetDefaultRepository()
        {
            return new EFDataLayer();
        }
    }
}