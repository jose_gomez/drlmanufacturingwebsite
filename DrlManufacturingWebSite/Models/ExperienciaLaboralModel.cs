﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DrlManufacturingWebSite.Models
{
    public class ExperienciaLaboralModel
    {
       
        public string Empresa { get; set; }
        [Display(Name = "A que se dedicá:")]
        public string DedicacionEmpresa { get; set; }
         [Display(Name = "Tareas Realizadas:")]
        public string TareasRealizadas { get; set; }
        public double Sueldo { get; set; }
        public DateTime Desde { get; set; }
        public DateTime hasta { get; set; }
         [Display(Name = "Causa de retiro:")]
        public string CausaRetiro { get; set; }
         [Display(Name = "Telefono de la empresa:")]
        [DataType(DataType.PhoneNumber, ErrorMessage="Por favor digite un numero de telefono valido")]
        public string TelEmpresa { get; set; }
    }
}
