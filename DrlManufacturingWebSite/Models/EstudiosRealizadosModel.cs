﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DrlManufacturingWebSite.Models
{
    public class EstudiosRealizadosModel
    {
        public EstudiosRealizadosModel()
        {
            this.Idiomas = new List< Idiomas>();
        }
        public string PrimariosCompletos { get; set; }
        public string SecundariosCompletos { get; set; }
        public string UniversitariosCompletos { get; set; }
        public string DiplomadoCompleto { get; set; }
        public string PostgradoCompleto { get; set; }
        public string MaestriaCompleto { get; set; }
        public string ComputacionCompleto { get; set; }
        [Required(ErrorMessage="Por favor indique si esta estudiando actualmente")]
        public bool EstudiaActualmente { get; set; }
        public List<string> TitulosObtenidos { get; set; }
        public List<string> Programas { get; set; }
        public List<Idiomas> Idiomas { get; set; }
        public string Dia { get; set; }
        public string Hora { get; set; }
        public int UserID { get; set; }
    }

    public class Idiomas
    {
        public string Idioma { get; set; }
        public int? Dominio { get; set; }
    }
}