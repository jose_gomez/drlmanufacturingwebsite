﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DrlManufacturingWebSite.Models
{
 public   class DeclaracionSaludModel
    {
        [Required(ErrorMessage="Por favor indique si sufre de algún padecimiento")]
        public bool IsPadecimientoSet { get; set; }

      [Required(ErrorMessage = "Por favor indique si sufre de alergías")]
        public bool IsAllergySet { get; set; }

        public string Padecimiento { get; set; }
        public string Allergy { get; set; }
        [Required(ErrorMessage = "Por favor indique si esta disponible para horas extras")]
        public bool IsAvailableForOvertime { get; set; }
        [Required(ErrorMessage = "Por favor indique si esta disponible para fines de semana")]
        public bool IsAvailableForWeekEnds { get; set; }
        public int UserId { get; set; }

        public bool Validate()
        {
            bool allergyValidated = false;
            bool padecimientosValidated = false;
            if (IsAllergySet || IsPadecimientoSet)
            {
                if (IsAllergySet)
                {
                    allergyValidated = !string.IsNullOrEmpty(Allergy);   
                }
                else
                {
                    allergyValidated = true;
                }
                if (IsPadecimientoSet)
                {
                    padecimientosValidated = !string.IsNullOrEmpty(Padecimiento);
                }
                else
                {
                    padecimientosValidated = true;
                }
               
            }
            else
            {
                allergyValidated = true;
                padecimientosValidated = true;
            }
            return (allergyValidated && padecimientosValidated);
        }
    }
}
