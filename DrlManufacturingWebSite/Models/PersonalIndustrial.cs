﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrlManufacturingWebSite.Models
{
   public class PersonalIndustrial
    {
        public bool Joyero { get; set; }
        public bool Tornero { get; set; }
        public bool Modelista { get; set; }
        public bool Pulidos { get; set; }
        public bool Fresador { get; set; }
        public bool SoldadorAceteleno { get; set; }
        public bool Grabador { get; set; }
        public bool Montador { get; set; }
        public bool Banglero { get; set; }
        public bool ElectronicaIndustrial { get; set; }
        public bool Mecanica { get; set; }
        public bool Abanileria { get; set; }
        public bool Electronica { get; set; }
        public bool  Otros { get; set; }
        public bool Refrigerador { get; set; }

    }
}
