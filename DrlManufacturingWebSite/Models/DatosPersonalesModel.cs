﻿using DrlManufacturingWebSite.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace DrlManufacturingWebSite.Models
{
    public class DatosPersonalesModel
    {

        [Required(ErrorMessage = "Por favor ingrese su nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Por favor ingrese su Apellido")]
        public string Apellido { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el nombre de la calle donde reside")]
        public string Calle { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el número de su residencía")]
        [Range(0, int.MaxValue, ErrorMessage="Por favor ingrese solo numeros")]
        public int NumeroCasa { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el nombre de su sector")]
        public string Sector { get; set; }

        [Required(ErrorMessage = "Por favor ingrese su localidad")]
        public string Localidad { get; set; }

        [Required(ErrorMessage = "Por favor ingrese su nacionalidad")]
        public string Nacionalidad { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el número de su telefono")]
        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el número de su celular")]
        [DataType(DataType.PhoneNumber)]
        public string Celular { get; set; }

        [Required(ErrorMessage = "Por favor ingrese su dirección de correo electronico")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage="Por favor ingrese su fecha de nacimiento")]
        [DataType(DataType.Date)]
        public DateTime FechaNacimiento { get; set; }

        [Required(ErrorMessage = "Por favor ingrese su numero de cedula")]
        [Cedula(ErrorMessage="Por favor digite una identificación valida")]
        public string Cedula { get; set; }

        public string Apodo { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el número de telefono de su contacto de emergencia")]
        [DataType(DataType.PhoneNumber)]
        public string TelEmergencia { get; set; }

        [Required(ErrorMessage = "Por favor ingrese su estado civil")]
        public string EstadoCivil { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el nombre del contacto para emergencias")]
        public string NombreContactoEmergencia { get; set; }


        public byte[] Foto { get; set; }



        public int UserID { get; set; }
    }
}
