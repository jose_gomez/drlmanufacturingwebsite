﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DrlManufacturingWebSite.Models
{
 public  class GrupoFamiliarModel
    {
        public int ID { get; set; }
        public string Conyugue { get; set; }
        public string ViveCon { get; set; }
        [Required(ErrorMessage="Por favor ingrese el total de personas a cargo. 0 para ninguno")]
        [Range(0, 20, ErrorMessage="Por favor ingrese solo números para el total de personas a cargo")]
        public int TotalPersonasACargo { get; set; }
        public string ActividadConyugue { get; set; }
        public string NombrePadre { get; set; }
        public string NombreMadre { get; set; }
        public List<string> Hermanos { get; set; }

        [Required(ErrorMessage = "Por favor ingrese la cantidad de hijos a cargo. 0 para ninguno")]
        [Range(0, 20,ErrorMessage = "Por favor ingrese la cantidad de hijos a cargo. 0 para ninguno.")]
        public int HijosACargo { get; set; }
        public List<int> EdadesHijos { get; set; }
        public int UserID { get; set; }
    }
}
