﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DrlManufacturingWebSite.Models
{
    public class UsersModel
    {
        private sbyte? _isEnabled;

        [Display(AutoGenerateField = false)]
        public int UserId { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "La dirección de correo es requerida")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(AutoGenerateField = false)]
        public int RoleId { get; set; }

        [Display(AutoGenerateField = false)]
        public string Role { get; set; }

        [Display(AutoGenerateField = false)]
        public sbyte? IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }

        }

        [Display(AutoGenerateField = false)]
        public DateTime? LastVisit { get; set; }

    }
}