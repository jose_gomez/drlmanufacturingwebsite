﻿use master
GO
IF not EXISTS (SELECT name FROM master.sys.databases WHERE name = N'paises')
Create Database Paises
GO
USE Paises;


/*Drop Tables if exist */

if (Exists (Select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'municipios'))
drop table municipios

if (Exists (Select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'provincias'))
drop table provincias

if (Exists (Select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'paises'))
drop table paises

--
-- Definition of table "paises"
--


CREATE TABLE Paises (
  IdPais int not null primary key identity,
  Pais nvarchar(50) NOT NULL,
) 

--
-- Definition of table "provincias"
--


CREATE TABLE provincias (
  IdProvincia int NOT NULL primary key identity,
  IdPais int  NULL foreign key references Paises(idpais),
  Provincia nvarchar(70) NULL,
  CONSTRAINT "fk_Provincias_Paises" FOREIGN KEY ("IdPais") REFERENCES "paises" ("IdPais") ON DELETE NO ACTION ON UPDATE NO ACTION
) 

--
-- Definition of table "municipios"
--
GO


CREATE TABLE municipios (
  IdMunicipio int NOT NULL Primary key identity,
  IdProvincia int  NULL foreign key references provincias(idProvincia) on delete no action on update no action,
  Municipio nvarchar(50)  NULL,
  CONSTRAINT fk_Municipios_Provincias1 FOREIGN KEY (IdProvincia) REFERENCES provincias("IdProvincia") 
) 

GO

Go

--
-- Dumping data for table "paises"
--

SET IDENTITY_INSERT dbo.paises ON
INSERT INTO paises (IdPais,Pais) VALUES 
 (1,'Republica Dominicana'),
 (2,'Estados Unidos');
 SET IDENTITY_INSERT dbo.paises OFF
 --
-- Dumping data for table "provincias"
--
SET IDENTITY_INSERT dbo.provincias ON
INSERT INTO provincias (IdProvincia,IdPais,Provincia) VALUES 
 (1,1,'San Juan'),
 (2,1,'Azua'),
 (3,1,'Bahoruco'),
 (4,1,'Barahona'),
 (5,1,'Dajabón'),
 (6,1,'Duarte'),
 (7,1,'El Seibo'),
 (8,1,'Elías Piña'),
 (9,1,'Espaillat'),
 (10,1,'Hato Mayor'),
 (11,1,'Hermanas Mirabal'),
 (12,1,'Independencia'),
 (13,1,'La Altagracia'),
 (14,1,'La Romana'),
 (15,1,'La Vega'),
 (16,1,'María Trinidad Sánchez'),
 (17,1,'Monseñor Nouel'),
 (18,1,'Montecristi'),
 (19,1,'Monte Plata'),
 (20,1,'Pedernales'),
 (21,1,'Peravia'),
 (22,1,'Puerto Plata'),
 (23,1,'Samaná'),
 (24,1,'San Cristóbal'),
 (25,1,'San José de Ocoa'),
 (26,1,'San Pedro de Macorís'),
 (27,1,'Sánchez Ramírez'),
 (28,1,'Santiago'),
 (29,1,'Santiago Rodríguez'),
 (30,1,'Santo Domingo'),
 (31,1,'Valverde'),
 (32,1,'Distrito Nacional');
SET IDENTITY_INSERT dbo.provincias OFF
 

--
-- Dumping data for table "municipios"
--

SET IDENTITY_INSERT dbo.municipios ON;
INSERT INTO municipios (IdMunicipio,IdProvincia,Municipio) VALUES 
 (3,2,'Estebanía'),
 (4,2,'Guayabal'),
 (5,2,'Las Charcas'),
 (6,2,'Las Yayas de Viajama'),
 (7,2,'Padre Las Casas'),
 (8,2,'Peralta'),
 (9,2,'Pueblo Viejo'),
 (10,2,'Sabana Yegua'),
 (11,2,'Tábara Arriba'),
 (12,3,'Neiba'),
 (13,3,'Galván'),
 (14,3,'Los Ríos'),
 (15,3,'Tamayo'),
 (16,3,'Villa Jaragua'),
 (17,4,'Barahona'),
 (18,4,'Cabral'),
 (19,4,'El Peñón'),
 (20,4,'Enriquillo'),
 (21,4,'Fundación'),
 (22,4,'Jaquimeyes'),
 (23,4,'La Ciénaga'),
 (24,4,'Las Salinas'),
 (25,4,'Paraíso'),
 (26,4,'Polo'),
 (27,4,'Vicente Noble'),
 (29,5,'Dajabón'),
 (30,5,'El Pino'),
 (31,5,'Loma de Cabrera'),
 (32,5,'Partido'),
 (33,5,'Restauración'),
 (34,6,'San Francisco de Macorís'),
 (35,6,'Arenoso'),
 (36,6,'Castillo'),
 (37,6,'Eugenio María de Hostos'),
 (38,6,'Las Guáranas'),
 (39,6,'Pimentel'),
 (40,6,'Villa Riva'),
 (41,32,'Distrito Nacional'),
 (42,7,'El Seibo'),
 (43,7,'Miches'),
 (44,8,'Comendador'),
 (45,8,'Bánica'),
 (46,8,'El Llano'),
 (47,8,'Hondo Valle'),
 (48,8,'Juan Santiago'),
 (49,8,'Pedro Santana'),
 (50,9,'Moca'),
 (51,9,'Cayetano Germosén'),
 (52,9,'Gaspar Hernández'),
 (53,9,'Jamao al Norte'),
 (54,10,'Hato Mayor del Rey'),
 (55,10,'El Valle'),
 (56,10,'Sabana de la Mar'),
 (58,11,'Salcedo'),
 (59,11,'Tenares'),
 (60,11,'Villa Tapia'),
 (61,12,'Jimaní'),
 (62,12,'Cristóbal'),
 (63,12,'Duvergé'),
 (64,12,'La Descubierta'),
 (65,12,'Mella'),
 (66,12,'Postrer Río'),
 (67,13,'Higüey'),
 (68,13,'San Rafael del Yuma'),
 (69,14,'La Romana'),
 (70,14,'Guaymate'),
 (71,14,'Villa Hermosa'),
 (73,15,'La Concepción de La Vega'),
 (74,15,'Constanza'),
 (75,15,'Jarabacoa'),
 (76,15,'Jima Abajo'),
 (77,16,'Nagua'),
 (78,16,'Cabrera'),
 (79,16,'El Factor'),
 (80,16,'Río San Juan'),
 (81,17,'Bonao'),
 (82,17,'Maimón'),
 (83,17,'Piedra Blanca'),
 (84,18,'Montecristi'),
 (85,18,'Castañuela'),
 (86,18,'Guayubín'),
 (87,18,'Las Matas de Santa Cruz'),
 (88,18,'Pepillo Salcedo'),
 (89,18,'Villa Vásquez'),
 (90,19,'Monte Plata'),
 (91,19,'Bayaguana'),
 (92,19,'Peralvillo'),
 (93,19,'Sabana Grande de Boyá'),
 (94,19,'Yamasá'),
 (95,20,'Pedernales'),
 (96,20,'Oviedo'),
 (97,21,'Baní'),
 (98,21,'Nizao'),
 (99,22,'Puerto Plata'),
 (100,22,'Altamira'),
 (101,22,'Guananico'),
 (102,22,'Imbert'),
 (103,22,'Los Hidalgos'),
 (104,22,'Luperón'),
 (105,22,'Sosúa'),
 (106,22,'Villa Isabela'),
 (107,22,'Villa Montellano'),
 (108,23,'Samaná'),
 (109,23,'Las Terrenas'),
 (110,23,'Sánchez'),
 (111,24,'San Cristóbal'),
 (112,24,'Bajos de Haina'),
 (113,24,'Cambita Garabito'),
 (114,24,'Los Cacaos'),
 (115,24,'Sabana Grande de Palenque'),
 (116,24,'San Gregorio de Nigua'),
 (117,24,'Villa Altagracia'),
 (118,24,'Yaguate'),
 (119,25,'San José de Ocoa'),
 (120,25,'Rancho Arriba'),
 (121,25,'Sabana Larga'),
 (122,1,'San Juan de la Maguana'),
 (123,1,'Bohechío'),
 (124,2,'Azua de Compostela'),
 (125,1,'El Cercado'),
 (126,1,'Juan de Herrera'),
 (127,1,'Las Matas de Farfán'),
 (128,1,'Vallejuelo'),
 (129,26,'San Pedro de Macorís'),
 (130,26,'Consuelo'),
 (131,26,'Guayacanes'),
 (132,26,'Quisqueya'),
 (133,26,'Ramón Santana'),
 (134,26,'San José de Los Llanos'),
 (135,27,'Cotuí'),
 (136,27,'Cevicos'),
 (137,27,'Fantino'),
 (138,27,'La Mata'),
 (139,28,'Santiago'),
 (140,28,'Bisonó'),
 (141,28,'Jánico'),
 (142,28,'Licey al Medio'),
 (143,28,'Puñal'),
 (144,28,'Sabana Iglesia'),
 (145,28,'San José de las Matas'),
 (146,28,'Tamboril'),
 (147,28,'Villa González'),
 (148,29,'San Ignacio de Sabaneta'),
 (149,29,'Los Almácigos'),
 (150,29,'Monción'),
 (152,30,'Boca Chica'),
 (153,30,'Santo Domingo Este'),
 (154,30,'Los Alcarrizos'),
 (155,30,'Pedro Brand'),
 (156,30,'San Antonio de Guerra'),
 (157,30,'Santo Domingo Norte'),
 (158,30,'Santo Domingo Oeste'),
 (159,31,'Mao'),
 (160,31,'Esperanza'),
 (161,31,'Laguna Salada');

SET IDENTITY_INSERT dbo.municipios OFF



 Go