﻿

function verifyUserName(userName) {
    if ($) {
        if (userName.value.trim().length == 0)
            return;
        $.ajax(
            {
                url: "/AjaxRequest/VerifyUserName?userName=" + userName.value,
                success: function (data) {
                    if (data.Exists) {
                        SetMessage(userName, 'red', 'Este nombre de usuario ya esta en uso');
                    }
                    else {
                        SetMessage(userName, 'green', 'Usuario Disponible');
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            }
            );
    }
}


function SetMessage(targetElement, messageColor, message) {
    $(targetElement).next('div').remove();
    $(targetElement).after("<div id='name-message' >" + message + "</div>");
    $(targetElement).next('div').css('color', messageColor);
    $(targetElement).next('div').effect("highlight", {}, 2000);
}



function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    $('#pictures').empty();
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

        // Only process image files.
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function (theFile) {
            return function (e) {
                // Render thumbnail.
                document.getElementById('list').innerHTML = '';
                var span = document.createElement('span');
                span.innerHTML = ['<img class="thumb" src="', e.target.result,
                  '" title="', escape(theFile.name), '"/>'
                ].join('');
                document.getElementById('list').insertBefore(span, null);
            };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
    }
}

function confirmDeleteElement(element) {

}

$(document).ready(

        function () {
            if (document.getElementById('FotoString')) {
                document.getElementById('FotoString').addEventListener('change', handleFileSelect, false);
            }

            if ($('.datepicker')) {
                $('.datepicker').datepicker({
                    showOn: "button",
                    buttonImage: "/images/calendar.gif",
                    buttonImageOnly: true,
                    buttonText: "Select date",
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'mm-dd-yy',
                    minDate: new Date(1960, 1, 1),
                    maxDate: new Date().getFullYear(),
                    yearRange: '1960:c'
                });
            }

            $('.carousel').carousel({
                interval: 5000
            });

            $(".datepicker").change(function () {
                //var date = $(this).val(); // replace this by $(this).val() to get your date from the input
                //var validate = Date.parse(date);
                //if (isNaN(validate)) {
                //    SetMessage($(this), 'red', 'Por favor introduzca una fecha valida');
                //}
                //else {
                //    SetMessage($(this), 'green', '');
                //}

            });

            $(".datepicker").attr('readonly', 'readonly');

            $(".datepicker").css("cursor", "default");
            
            

            $('.deleteButton').click(function () {
                if (confirm("¿Esta seguro de que desea borrar este elemento?")) {
                    $(this).closest('li').remove();

                }
            });

            if ($('#HijosACargo')) {
                $('#HijosACargo').change(
                 function () {
                     if ($.isNumeric($('#HijosACargo').val())) {
                         var count = $('#HijosACargo').val();
                         if (count > 10) {
                             count = 10;
                         }
                         $('#childrenAges').empty();
                         for (i = 0; i < count; i++) {
                             if ($('#childrenAges')) {
                               
                                 $('#childrenAges').append(
                                        "<input type='text' name='EdadesHijos' class='edades' style = 'border-style:solid;border-width: 1px;' />");
                             }
                         }
                     }
                 });
            }

        }
    );
