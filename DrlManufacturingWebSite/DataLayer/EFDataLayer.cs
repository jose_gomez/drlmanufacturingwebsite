﻿using DrlManufacturingWebSite.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using DrlManufacturingWebSite.Entities;
using System.Web;
using DrlManufacturingWebSite.Models;

namespace DrlManufacturingWebSite.DataLayer
{
    public class EFDataLayer : IUsersRepository, IDatosPersonalesRepository, IDeclaracionSaludRepository, IGrupoFamiliarRepository, IEstudiosRealizadosRepository
    {

        public Models.UsersModel GetUserByUsername(string username)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            var user = entities.users.Where(x => x.UserName == username).Select(
                    u => new UsersModel()
                    {
                        UserId = u.UserID,
                        UserName = u.UserName,
                        IsEnabled = u.IsEnabled,
                        LastVisit = u.LastVisit,
                        Password = u.Password,
                        Role = u.role.Role1,
                        RoleId = u.role.RoleID
                    }
                ).FirstOrDefault();

            return user;
        }

        public Models.UsersModel GetUserByID(int userID)
        {
            throw new NotImplementedException();
        }

        public bool IsEnabled(string username)
        {
            throw new NotImplementedException();
        }

        public void SaveUser(Models.UsersModel user)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            var userToSave = entities.users.Where(u => u.UserName == user.UserName).FirstOrDefault();
            if (userToSave != null)
            {
                userToSave.Password = user.Password;
                userToSave.LastVisit = user.LastVisit;
                userToSave.IsEnabled = user.IsEnabled;
                userToSave.Email = user.Email;
            }
            else
            {
                userToSave = new user();
                userToSave.UserName = user.UserName;
                userToSave.Password = user.Password;
                userToSave.RoleID = user.RoleId;
                userToSave.Email = user.Email;
                userToSave.LastVisit = user.LastVisit;
                userToSave.IsEnabled = user.IsEnabled;
                userToSave.CreateDate = DateTime.Now;
                entities.users.Add(userToSave);
            }
            entities.SaveChanges();
        }

        public bool DisableUser(Models.UsersModel user)
        {
            throw new NotImplementedException();
        }

        public bool EnableUser(Models.UsersModel user)
        {
            throw new NotImplementedException();
        }


        public UsersModel GetUserByEmail(string email)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            var user = entities.users.Where(x => x.Email == email).Select(
                    u => new UsersModel()
                    {
                        UserId = u.UserID,
                        UserName = u.UserName,
                        IsEnabled = u.IsEnabled,
                        LastVisit = u.LastVisit,
                        Password = u.Password,
                        Role = u.role.Role1,
                        RoleId = u.role.RoleID
                    }
                ).FirstOrDefault();
            return user;
        }

        public DatosPersonalesModel GetDatosPersonales(int userID)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            var datos = entities.datospersonales.Where(d => d.UserID == userID).Select(c => new DatosPersonalesModel()
            {
                Apellido = c.Apellido,
                Calle = c.Calle,
                Celular = c.Celular,
                Email = c.Email,
                Apodo = c.Apodo,
                Cedula = c.Cedula,
                EstadoCivil = c.EstadoCivil,
                FechaNacimiento = c.FechaNacimiento,
                Foto = c.foto,
                Localidad = c.Localidad,
                Nacionalidad = c.Nacionalidad,
                Nombre = c.Nombre,
                NombreContactoEmergencia = c.NombreContactoEmergencias,
                NumeroCasa = c.NoCasa,
                Sector = c.Sector,
                Telefono = c.Telefono,
                TelEmergencia = c.TelefonoEmergencia,
                UserID = (int)c.UserID
            }).FirstOrDefault();

            return datos;
        }

        public void SaveDatosPersonales(DatosPersonalesModel model)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");

            var datos = entities.datospersonales.Where(d => d.UserID == model.UserID).FirstOrDefault();
            bool exists = datos != null;
            if (datos == null)
            {
                datos = new datospersonale();
            }
           
            datos.Nombre = model.Nombre;
            datos.Apellido = model.Apellido;
            datos.Cedula = model.Cedula;
            datos.Celular = model.Celular;
            datos.Apodo = model.Apodo;
            datos.Calle = model.Calle;
            datos.Email = model.Email;
            datos.EstadoCivil = model.EstadoCivil;
            datos.FechaNacimiento = model.FechaNacimiento;
            datos.Localidad = model.Localidad;
            datos.Nacionalidad = model.Nacionalidad;
            datos.Sector = model.Sector;
            datos.foto = model.Foto;
            datos.TelefonoEmergencia = model.TelEmergencia;
            datos.NombreContactoEmergencias = model.NombreContactoEmergencia;
            datos.NoCasa = model.NumeroCasa;
            datos.Telefono = model.Telefono;
            datos.UserID = model.UserID;
            if (!exists)
            {
                datos.CreateDate = DateTime.Now;
                entities.datospersonales.Add(datos);
            }
            entities.SaveChanges();

        }

        public DeclaracionSaludModel GetDeclaracionSalud(int userId)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            DeclaracionSaludModel mod = new DeclaracionSaludModel();
            var dec = (from c in entities.declaracionsaluds
                       join d in entities.disponibilidads on c.UserID equals d.UserID
                       where c.UserID == userId
                       select new DeclaracionSaludModel()
                       {
                           Allergy = c.Alergias,
                           IsAllergySet = c.TieneAlergias == 0 ? false : true,
                           IsPadecimientoSet = c.TienePadecimiento == 0 ? false : true,
                           Padecimiento = c.Padecimiento,
                           IsAvailableForOvertime = d.HorasExtras == 0?false:true,
                           IsAvailableForWeekEnds = d.FinesSemana == 0 ? false : true
                       }).FirstOrDefault();

         
           
            return dec;
        }

        public void SaveDeclaracionSalud(DeclaracionSaludModel model)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            bool existingDeclaration = false;
            bool existingDisponibilidad = false;
            declaracionsalud dec = null;
            disponibilidad dis = null;
            dec = entities.declaracionsaluds.Where(d => d.UserID == model.UserId).FirstOrDefault();
            if (dec == null)
            {
                dec = new declaracionsalud();
            }
            else
                existingDeclaration = true;

            dec.Padecimiento = model.Padecimiento;
            dec.TieneAlergias = model.IsAllergySet?1:0;
            dec.TienePadecimiento = model.IsPadecimientoSet ? 1 : 0;
            dec.Alergias = model.Allergy;
            dec.UserID = model.UserId;

            dis = entities.disponibilidads.Where(di => di.UserID == model.UserId).FirstOrDefault();
            if (dis == null)
            {
                dis = new disponibilidad();
            }
            else
                existingDisponibilidad = true;
          
            dis.FinesSemana = model.IsAvailableForWeekEnds ? 1 : 0;
            dis.HorasExtras = model.IsAvailableForOvertime ? 1 : 0;
            dis.UserID = model.UserId;

            if (!existingDeclaration)
                entities.declaracionsaluds.Add(dec);
            if (!existingDisponibilidad)
                entities.disponibilidads.Add(dis);

            entities.SaveChanges();

        }

        public GrupoFamiliarModel GetGrupoFamiliar(int userID)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            var grupo = entities.grupofamiliars.Where(g => g.UserID == userID).Select(g => new GrupoFamiliarModel()
            {
                 ActividadConyugue = g.ActividadConyugue,
                 Conyugue = g.NombreConyugue,
                 HijosACargo = (int)g.HijosACargo,
                 TotalPersonasACargo = (int)g.PersonasACargo,
                 NombreMadre = g.NombreMadre,
                 NombrePadre = g.NombrePadre,
                 ID=g.idgrupofamiliar
            }).FirstOrDefault();

            if (grupo != null)
            {
                grupo.EdadesHijos = entities.edadeshijos.Where(e => e.idgrupofamiliar == grupo.ID).Select(e=>e.edad).ToList();
                grupo.Hermanos = entities.hermanos.Where(h => h.IdgrupoFamiliar == grupo.ID).Select(h => h.Nombre).ToList();
            }

            return grupo;
        }

        public void SaveGrupoFamiliar(GrupoFamiliarModel model)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            drlwebdbEntities delEntities = new drlwebdbEntities("name=drlwebdbEntities");
            hermano her = new hermano();
            edadeshijo eds = new edadeshijo();
            var grupo = entities.grupofamiliars.Where(g => g.UserID == model.UserID).FirstOrDefault();
            bool existingGroup = false;
            if(grupo == null)
            {
                grupo = new grupofamiliar();
            }
            else
                existingGroup = true;
            grupo.HijosACargo = model.HijosACargo;
            grupo.PersonasACargo = model.TotalPersonasACargo;
            grupo.NombreMadre = model.NombreMadre;
            grupo.NombrePadre = model.NombrePadre;
            grupo.ViveCon = model.ViveCon;
            grupo.NombreConyugue = model.Conyugue;
            grupo.ActividadConyugue = model.ActividadConyugue;
            grupo.UserID = model.UserID;
            if (!existingGroup)
            {
                entities.grupofamiliars.Add(grupo);
            }

            var edades = delEntities.edadeshijos.Where(e => e.idgrupofamiliar == grupo.idgrupofamiliar).ToList();
            var hers = delEntities.hermanos.Where(h => h.IdgrupoFamiliar == grupo.idgrupofamiliar).ToList();
            foreach (var x in edades)
            {
                delEntities.edadeshijos.Remove(x);
            }

            foreach (var y in hers)
            {
                delEntities.hermanos.Remove(y);
            }
            delEntities.SaveChanges();
            foreach (string s in model.Hermanos)
            {
                bool add = grupo.hermanos.Where(h => h.Nombre == s).Any();
                if (!add)
                {
                    grupo.hermanos.Add(new hermano()
                    {
                        IdgrupoFamiliar = grupo.idgrupofamiliar,
                        Nombre = s
                    });
                }
               
            }

            foreach(int edad in model.EdadesHijos)
            {
                bool add = grupo.edadeshijos.Where(ed => ed.edad == edad).Any();
                if (!add)
                {
                    grupo.edadeshijos.Add(new edadeshijo()
                    {
                        idgrupofamiliar = grupo.idgrupofamiliar,
                        edad = edad   
                    });
                }
              
            }
            entities.SaveChanges();
        }

        public EstudiosRealizadosModel GetEstudiosRealizados(int userID)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            var estudios = entities.estudiosrealizados.Where(est => est.userID == userID).Select(es => new EstudiosRealizadosModel()
            {
                ComputacionCompleto = es.computacion,
                DiplomadoCompleto = es.diplomadocompleto,
                MaestriaCompleto = es.maestriacompleto,
                PostgradoCompleto = es.postgradocompleto,
                PrimariosCompletos = es.primarioscompletos,
                EstudiaActualmente = es.estudiaActualmente == 0 ? false : true,
                SecundariosCompletos = es.secundariocompleto,
                Dia = es.dia.ToString(),
                Hora = es.horario
            }).FirstOrDefault();
            if (estudios != null)
            {
                estudios.Idiomas = entities.idiomas.Where(id => id.userID == userID).Select(i=> new Idiomas(){
                 Dominio = int.Parse(i.nivel),
                  Idioma = i.idioma1
                }).ToList();

                estudios.TitulosObtenidos = entities.titulosobtenidos.Where(tit => tit.userID == userID).Select(ti=> ti.titulo).ToList();
                estudios.Programas = entities.manejoprogramas.Where(m => m.userID == userID).Select(ma => ma.nombrePrograma).ToList();
            }
            return estudios;
        }

        public void SaveEstudiosRealizados(EstudiosRealizadosModel model)
        {
            drlwebdbEntities entities = new drlwebdbEntities("name=drlwebdbEntities");
            bool existing = false;
            estudiosrealizado estudios = entities.estudiosrealizados.Where(est => est.userID == model.UserID).FirstOrDefault();
            
            if (estudios == null)
            {
                estudios = new estudiosrealizado();
            }
            else
                existing = true;
            estudios.computacion = model.ComputacionCompleto;
            estudios.diplomadocompleto = model.DiplomadoCompleto;
            estudios.estudiaActualmente = model.EstudiaActualmente?1:0;
            estudios.horario = model.Hora;
            estudios.dia = DateTime.Parse(model.Dia);
            estudios.maestriacompleto = model.MaestriaCompleto;
            estudios.primarioscompletos = model.PrimariosCompletos;
            estudios.secundariocompleto = model.SecundariosCompletos;
            estudios.universitariocompleto = model.UniversitariosCompletos;
            estudios.userID = model.UserID;
            foreach (string s in model.Programas)
            {
                entities.manejoprogramas.Add(new manejoprograma()
                {
                    nombrePrograma = s,
                    userID = model.UserID
                });
            }
            foreach (Idiomas y in model.Idiomas)
            {
                entities.idiomas.Add(new idioma()
                {
                    idioma1 = y.Idioma,
                    nivel = y.Dominio.ToString()
                });
            }
            foreach (var z in model.TitulosObtenidos)
            {
                entities.titulosobtenidos.Add(new titulosobtenido()
                {
                    titulo = z,
                    userID = model.UserID
                });
            }
        }
    }
}