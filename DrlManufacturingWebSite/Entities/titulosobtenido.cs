//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DrlManufacturingWebSite.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class titulosobtenido
    {
        public int idtitulosObtenidos { get; set; }
        public string titulo { get; set; }
        public int userID { get; set; }
    
        public virtual user user { get; set; }
    }
}
