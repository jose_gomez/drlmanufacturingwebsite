﻿using System;
using System.Collections.Generic;
using DrlManufacturingWebSite.Entities;
using DrlManufacturingWebSite.Models;
using System.Linq;
using System.Text;

namespace DrlManufacturingWebSite.Contracts
{
  public  interface IDeclaracionSaludRepository
    {
       DeclaracionSaludModel GetDeclaracionSalud(int userId);
       void SaveDeclaracionSalud(DeclaracionSaludModel model);
    }
}
