﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrlManufacturingWebSite.Models;

namespace DrlManufacturingWebSite.Contracts
{
  public  interface IUsersRepository
    {
        UsersModel GetUserByUsername(string username);
        UsersModel GetUserByID(int userID);
        UsersModel GetUserByEmail(string email);
        bool IsEnabled(string username);
        void SaveUser(UsersModel user);
        bool DisableUser(UsersModel user);
        bool EnableUser(UsersModel user);
       
    }
}
