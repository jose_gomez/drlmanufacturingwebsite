﻿using DrlManufacturingWebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrlManufacturingWebSite.Contracts
{
  public  interface IDatosPersonalesRepository
    {
        DatosPersonalesModel GetDatosPersonales(int userID);
        void SaveDatosPersonales(DatosPersonalesModel model);
    }
}
