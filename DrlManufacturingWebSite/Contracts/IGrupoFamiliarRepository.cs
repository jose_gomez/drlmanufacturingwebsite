﻿using DrlManufacturingWebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrlManufacturingWebSite.Contracts
{
  public  interface IGrupoFamiliarRepository
    {
      GrupoFamiliarModel GetGrupoFamiliar(int userID);
      void SaveGrupoFamiliar(GrupoFamiliarModel model);
    }
}
