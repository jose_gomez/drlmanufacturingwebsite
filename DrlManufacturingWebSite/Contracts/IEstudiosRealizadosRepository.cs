﻿using DrlManufacturingWebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrlManufacturingWebSite.Contracts
{
    interface IEstudiosRealizadosRepository
    {
        EstudiosRealizadosModel GetEstudiosRealizados(int userID);
        void SaveEstudiosRealizados(EstudiosRealizadosModel model);
    }
}
