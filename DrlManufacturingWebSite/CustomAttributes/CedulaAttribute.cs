﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Web.UI;
using DrlManufacturingWebSite.CustomValidators;

namespace DrlManufacturingWebSite.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class Cedula : ValidationAttribute
    {

        private readonly CedulaValidator _cedulaValidator = new CedulaValidator();

        /// <summary>
        /// Determines if a string value is a valid Cédula.
        /// </summary>
        /// <param name="value">The string to validate.</param>
        /// <returns><code>true</code> if the string value is a valid Cédula, otherwise <code>false</code>.</returns>
        public override bool IsValid(object value)
        {
            return _cedulaValidator.Valid(value);
        }

    }
}
